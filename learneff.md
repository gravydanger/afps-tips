# gravy's AFPS tips
[(back to overview)](README.md)

## How to get maximum efficiency from practicing

The best way to practice is to spend as much time as possible playing the game, right? Well, no... at least not necessarily. If all you do is play the game, without trying to figure out where your strengths and weaknesses are (easier said than done, by the way), you will waste a lot of time, or at least not get nearly all of the potential value out of it. So, let's talk about how to go about practicing.

First off: when learning/practicing stuff, consistency always wins. For instance, playing for an hour every day of the week is better than playing seven hours once a week (and I'm not saying that seven hours a week is the absolute minimum that's useful, don't worry). To an extent, this is because your capacity to learn decreases the longer you're focusing on the same thing (you don't have infinite mental resources to focus), especially if there is frustration and other negativity involved. It's also because a lot of the "integration" of things you're learning happens during downtime, i.e. when you're not in the middle of fragging/playing anymore.

Of course, you don't need 100% consistency. If you can make a solid effort at playing for an hour a day (or even less, if need be), chances are you'll see a lot of improvement over a few months. (That said, improvement has diminishing returns. The first few months will typically see a lot more measurable improvement than any period later on, simply because the more you keep playing, the more it's all about the small details, and there are tons and tons of those.)

### Focus and its limitations

Focus is a great tool to amplify your learning in a narrowly defined region of your skillset. If you focus on dodging while playing, you'll learn a lot more about dodging, and a lot faster than you normally would. In fact if you didn't focus on it at some point, you might never realize that it's important – who knows?

Even without focusing on specific things, you'll get better at all aspects of the game over time... but some of the more complex things are hard to learn without paying attention to them, so certain types of improvements will take a very long time if you don't focus on them specifically. Having said that – why did I say there are limitations?

Two reasons. Number one: whatever you focus on is subject to critical analysis in your mind. If you have big goals and high standards, your focus is a doubly-edged sword, because if you find that improvement is slower than you expect of yourself, you'll get frustrated (see also my article about [tilting](untilt.md)). Number two: you can really only focus on one thing at a time, and if you switch focus between two things, there's a lot of friction that wastes mental resources, to the point that neither thing actually benefits. Imagine having to switch every single minute between writing a PhD thesis and preparing a five-course meal from scratch... yeah, that's no good, is it? Same principle.

So, applying focus needs **prioritization** (so you know what to focus on on a given time, and how long to focus on it) and (if frustration is likely) **patience** and compassion with yourself. I mostly covered the latter in my article about tilting, so this will focus on the former. The big question is this: how do you know what to focus on?

### The "you don't know that you don't know" problem

With AFPS, even once you've gotten a good grasp on the basics (of which there are many), the devil is still in the details. There are so many details that you couldn't possibly become aware of all of them out at the same time, and probably not within the first months or even years of playing, either. So, one of the biggest potential issues is that you'll be focusing on comparably unimportant things, simply because you're not aware of the real issues. Maybe focusing on those unimportant things will still help you, but not nearly as much as the thing you don't even know about...

In a similar vein, maybe you even know that you have certain weaknesses but you may not know what would be the best way of addressing them.

In both cases, there are really only two ways to tackle this. One is to get feedback from people who are better than you, the other is to experiment with different approaches and hopefully get insight from that. Both are not silver bullets, unfortunately. If you don't have the right ideas for experimentation, the outcomes will not give you the information you need; and feedback from others is only as good as their ability to detect what is affecting your performance the most. Some people are much better at that than others, and it's not necessarily obvious which people fall in which category.

If you are going to get feedback, try to get it from as many different people as possible. Most likely, patterns will emerge, and if those patterns are "you need a 1000 Hz monitor" or "you need this exact config", you can safely ignore them. A candidate for good feedback is anything concrete enough that you can easily verify that it affects your gameplay... and among those, focus on the ones that depend the least on stuff like raw aim (because that is something that improves over time and usually doesn't need focused practice).

Don't discount small details! One tiny mistake can make your situation in a match much worse. Of course mistakes will always happen, but the more you learn to avoid them, the better your chances. Again, this is something that develops over time, and all you need to realize is that you're making the mistakes; the awareness is enough to help you move on from them.

#### The "you know everything" problem

Sometimes it feels like you're doing everything right and still you lose. You'll be tempted to blame everything on aim, but if you have okay aim, that's almost certainly the wrong thing to focus on. There is something you're missing, trust me. It might not even be one big thing, but many small things. Start looking. When you ask for feedback, actually listen and *verify* it. If you think that this doesn't apply to you, it applies to you all the more.

#### I'll give feedback btw

If you're not at diamond level or something, I can probably point you towards some issues I see in your games. Send me recordings and I'll check them out (if I have time... I might say no. It's nothing personal, of course, and I absolutely don't mind you asking, either way).

### Making up your mind

Something I see over and over again (and I may or may not be guilty of this myself) is that fellow players can't decide which thing to focus on – remember: you can only focus on one thing at a time, and switching back and forth takes extra mental effort that reduces the effectiveness of focus by a lot. For this reason, whenever you're undecided between a few option of what to focus on, pick *any one* of the options and stick to it for *at least* 30 minutes of playing. Only after that should you consider shifting your focus on anything else, *even if* you have a vague notion that you may not be focusing on the optimal thing right now; otherwise you'll be prone to changing your mind way too often and it will take away from the consistency that makes focus actually do its job.

I literally can't think of any reason to ever break this rule... so, don't. Hey, the worst case is that you waste 30 minutes. Shifting focus more rapidly might end up wasting *all* of the time you put into playing.

### Choosing the right level of challenge

You might already have realized (or heard from others) that playing against someone who is *much* better than you is not the best use of your time. Unless you hate yourself, in which case you might feel like getting destroyed hard is inherently a useful thing to experience. This, of course, is complete nonsense.

Getting better is a journey:

* Initially you'll struggle against fairly weak players because you have absolutely no idea what you're doing.
* At some point you'll start getting a sense of the basics and start to, more often than not, dominate players who are on the level you started out on, however slightly more advanced players will give you a lot of trouble and you'll tend to lose. However, you've already learned things that will help you against these players, too.
* This cycle repeats. At each level (of which there are too many to count), you'll learn a few things that will help you have an easier time rise up and contribute to you being able to hold your own against the next level of opponent at some, while missing some more things that will make for most of the struggle against that next level.

What's great about this is that you don't have to learn to do everything right at the same time, so the development of your skills has a natural pace. On the other hand, you'll get used to relying on your opponents' more basic mistakes, and later on you'll find that more advanced opponents don't make the same mistakes, so this isn't perfect, either. So, how do you get the best of both worlds?

The answer is to play against opponents on all levels. Ideally most of your matches would be against players who are roughly on your level, then some against lower skilled opponents to allow you to experiment with weird ideas without getting absolutely stomped as a result, and finally some against better players so they can show you the mistakes you're still making. In practice, of course, it isn't quite as straightforward as that – not least because it will always be *easy* to see what you're doing wrong – but the general idea is sound.

If you play against a much, much better opponent, this breaks down a little: they'll just keep destroying you for so many different reasons, it will be borderline impossible for you to even focus on doing anything worthwhile except suicide missions which might help you get a few random kills but not necessarily actually improve your decision-making; in fact, you might get too focused on the kills that happened to work out, even though they worked out for the wrong reasons (random chance, over-reliance on godlike aim, etc.).

In addition to picking opponents that are not too far from you in terms of skill, another thing that can help is playing mock matches with additional restrictions so you can focus on certain aspects, particularly mechanics. For example, if both of you agree to not use rocket launcher, that's not exactly a realistic scenario but it will force you to focus more on the other weapons which might (again: for example) lead you to use lightning gun in more situations where it's appropriate, and also to force more mid-range than close-range encounters. Or both of you might just practice hide-and-seek, or running around pre-aiming and "pinging" each other with MG but not actually going for kills. Decisions made in such restricted games will not directly translate into proper games, but many other things will.

Specifically, if you need to work on your core mechanics (movement, weapon choices, dodging, ...), don't even bother playing duels; play public games (FFA, TDM etc.) instead. They have a much higher density of fights, so you'll spend a lot more time on the thing you want to practice. Duel practice should be mostly about getting better at thinking with portals, if you'll forgive the crossover. In fact, in theory you can even practice timing items in public games.

Want to drill it down even further? Well, you could practice timings with specialized tools. There is a dedicated strafe jump trainer on the web. There are aim trainers (though they're not a silver bullet; I'll be writing a separate article about aim trainers in the future). There are basic shooters like Counter-Strike where you can think more about general positioning and angles and don't have to worry nearly as much about weapon choices and complicated movement.

### Let it flow

One of the most important concepts in learning and general cognitive performance is the state of "flow", a term coined by psychologist Mihály Csíkszentmihályi (you don't have to memorize his name, don't worry) and also often called "the zone". Briefly, it says that if there's no challenge you're bored and if there's too much challenge you're frustrated or anxious, and there's a window in between the two where you're challenged just enough but not too much. If you stay in this window for a while, you get fully immersed in the experience, you enjoy it more, and you maximize the efficiency of learning. In a game like Quake it's impossible to stay in this state all the time, because the level of challenge can change in the blink of an eye... but still, the more you target the right level of challenge, the better your chances to experience times at which you just magically get better without it even feeling like effort.

By the way, the more you treat each match as an interesting experience or a puzzle rather than a tool to get better, the easier it becomes to reach a flow state... but that's easier said than done, of course, especially if you have high standards for yourself (again, refer to my article about tilting).

#### Don't "think hard"

During matches, what you don't want to do is frantic thinking and "trying hard" to come up with fancy plans. Think, yes. Plan, yes. Don't do it with a sense of urgency or panic, though. You don't want to get into the habit of feeling panicked during matches. If you can't come up with anything useful, that is absolutely fine. You can't win every match. Just fall back on bread-and-butter things that you already know and be open to ideas coming to you – and if they don't come, that's fine, too.

What you want to do if you experience a match where you don't get good ideas is to review it later on (if possible). Then you have all the time in the world to pause it and explore different thoughts and ideas... with zero stress.

Yes, this takes more time, but it keeps all of the self-inflicted negativity out of your actual match, and you still get to practice thinking the right thoughts, in an environment where you're not getting distracted by having to aim and hide and run and pay attention, all at once. It might not seem like it, but doing this (more than once, I might add) will actually make it easier to think better during future matches.

Some time in the future I'll be posting a separate article about how to analyze/troubleshoot past matches.

### Final remarks

I deliberately excluded specific pointers in this article, but I will write separate articles later that will cover an overall roadmap for learning AFPS and a guide for figuring out what to pay attention to when you're not making a lot of progress to hopefully figure out new things to focus on.

Practice makes perfect, but not all practice does, and no amount of practice that you don't actually end up doing will do anything useful for you. Put in the time, choose wisely what to do with it, keep looking for what you're not seeing (including the minor stuff)... and you'll go far. Now hop to it!
