# gravy's AFPS tips
[(back to overview)](README.md)

## The most important thing to focus on to get good

Ask this question in an AFPS community and you'll get many, many different
answers. However, there is really only one thing you need... let's try and
get that figured out.

Just to be clear: I'll be talking about Quake from now on, but really this
refers to any AFPS, and to some extent to any other multiplayer game.

### The basics of learning

Quake is a journey of learning. There is a bunch of basic knowledge you need,
of course, but the majority of what you learn is hard to nail down: it's the
experience that lets you make split-second decisions that get ever so slightly
better over time, without even specifically making any plans to work on them.

Like much of the learning in real life, much of this isn't obtained by reading
through a list of factoids and memorizing them - it's a mixture of trying
things, failing at things, trying different things, figuring out better ways,
seeing what others do, looking at others' ideas, and so forth. Much of the
actual process of improvement is based on this, but happens silently, without
you doing anything in particular. Imagine going to a huge shopping mall for the
many things you need, on a regular basis - at some point you'll know its layout
very well, you know where to park, which routes to take, when to avoid going
there if you don't have to, and so forth. You never set out to memorize all of
that - it happened along the way, just by going there and stumbling your way
through it.

This is Quake, too. Some of the things you learn, you'll be picking up from
advice and watching others (consulting the shopping mall's map)... and many
other things you'll learn on your own just by playing. 

So, let's say you don't seem to be improving... didn't I just claim that
everything will happen all by itself? What gives?

### The basics of... not learning

The process of learning has several ancient enemies. One of them is "not being
interested", but if you're reading something like this I'm going to assume that
that's not your problem (for now, at least). The other big one is *stress*.
What exactly do I mean by that?

To be clear, I'm talking about "stress" in the most general sense possible.
Most of the time, when people say "stress", they mean working too much, having
too many things to handle, that kind of thing. And that's part of what I mean
here, but "stress" has a more general meaning, too: anything that fires up the
sympathetic nervous system, the part of your nervous system that gets the blood
pumping, amps up your heart rate, makes you a little jittery... even if it's
just a little bit of all of that. In any case, this is the mechanism that preps
your body for reacting to a "potentially dangerous situation" - where you need
to be able to run away, possibly, and not spend too much time thinking things
through or taking your time and being objective and impartial. Quite literally,
stress takes away your ability to think and focus... and to learn, because
learning invariably requires at least a basic amount of focus.

Now, typically, people think that stress comes from the outside world. Your
environment is asking too much of you; the result: stress! While it's true that
there's a link between the two, in reality there's a missing step: your
environment is being demanding, you feel overwhelmed and *then* stress. Well,
guess what, you can feel overwhelmed without any outside help, too... and so
stress is something you can create all by yourself, in the privacy of your own
mind. Unintentionally, of course!

Ever wonder why some people seem to be learning so much more quickly than
others? Well, to some extent we're all a little different, of course... but
still people get stuck at very different levels, for no apparent reason (other
than being stressed out). If one person improves very quickly and then hits a
brick wall, and another person hits the same brick wall but much sooner,
*probably* talent isn't the only variable in that particular equation, right?

What creates stress? Conflict. Every beginner is overwhelmed by Quake, but the
people who can't handle being overwhelmed are the ones that get "too stressed"
to continue and leave. You'll often see players call these people "too weak" -
which is a terrible way to look at it, because hidden within it is the exact
same trap later down the road: you'll think yourself "too weak" to handle
further advancement, and then where does that leave you? Not in a position to
improve any further...

What do I mean by conflict?

A beginner comes to Quake and expects to get decent results after maybe an hour
or two of getting to know the basics, but they just keep getting stomped and
they don't understand why. Expectation conflicts with reality, and they're not
seeing an explanation. If they conclude that everyone is cheating or a
no-lifer, they'll leave. If they ask experienced players about it, they'll
understand that the game takes a fair amount of time to get basic proficiency
in, and then maybe they'll stay, implicitly accepting that they'll be investing
that amount of time.

After that, expectation and reality will conflict many, many more times -
that's life, and Quake even more so.

**If you can resolve the conflict, you will move forward. If you can't, you
will be stuck.**

It's a deceptively simple formula, but that's how it works. The devil is in the
details: what even is the conflict? What expectation is conflicting with what
part of reality? That in itself can be hard to figure out. And then - how do
you resolve the conflict? What do you need to understand so that space opens up
between expectation and reality for you to explore and grow in. Some of the
most intractable conflicts will be the ones that keep you "the stuckest". I'll
be writing a separate article about that, let's stick with the more
straightforward stuff for now.

### How to unstress

Most of the stress I see people get up to is because they get very focused on
some detail, for instance their lightning gun being bad. Sure, that's an
obstacle for improving... but if you tell yourself that it's the one thing
stopping you from being amazing at the game, you're creating pressure for
yourself: I need to get good at LG, or I'll be bad forever! (Maybe not quite
that extreme, but you get the idea.) This will weigh heavily on you, because
each time you're faced with an LG situation, you'll remember how *important*
this is, and it will create stress: the conflict between how good you want to
be and... how much you're not that good yet. As a result, you can't focus and
stay relaxed, and it will be that much harder to get anywhere.

Now, it's important to understand that the problem isn't that you're wrong -
getting better with LG is definitely extremely useful. The problem is that if
you pressure yourself into this particular corner, not only will the stress
make you get better slower, but *on top of that*, because all of your attention
is on your LG, progress will seem even slower than it is: you'll notice all of
the conflict (the less good LG) so much more thoroughly, that your perception
gets skewed towards "I'm always bad" rather than "I'm starting to get better
sometimes" - which will tempt you to pressure yourself even more.

The opposite is where to go. Ideally, you'll remove *all* of the pressure.
You'll just play, you'll notice slightly better situations and slightly worse
(even a lot worse) situations, but you'll shrug it off, and choose to enjoy the
good parts and take the bad parts along as part of the journey. Now, this is
easier said than done for many people, but this has to be your goal. I want to
be really clear about this: this isn't just *a* way to get better, it's *the*
way. You knew, of course, that taking the pressure off is a thing that might be
helpful maybe. You might not have believed that it's the most important thing,
period. Well, let me tell you now: **this is the most important thing to do.**

### Set the course, give it time

You might not be able to do it immediately. That's fine. This is a process, a
development, that you will make your way through. For now, all it needs to be
is your overall direction. You don't have to waste time feeling bad about
pressuring yourself. Just notice that you're doing it, and tell yourself that
you want to move away from this - no pressure, just an intention. You don't
have to try and put the power of The Force behind it, either.

If this is the only thing you change, I guarantee that you'll have a much
easier time, sooner than you might think. Before you know it, an LG that you
might be struggling with won't feel that daunting anymore, and you'll notice
being a little better every week - for no apparent reason.

If you're having trouble with this - I totally understand. Changing isn't
always easy. Still, I'll try to help you through it. That will be the focus of
my next article here.
