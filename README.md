# gravy's AFPS tips

This is a collection of tips for learning and improving in arena FPS games. It
focuses less on mechanics and more on strategies for learning and getting
"unstuck".

## Tips on learning and practicing

* [The most important thing to focus on to get good](gitgud.md)
* [How to deal with frustration and tilting](untilt.md)
* [How to get maximum efficiency from practicing](learneff.md)
* Aim trainers, yes or no?

## Topic guides/intros

* Arena FPS: road from newbie to experienced
* How to get into duels (1v1)
* Troubleshooting your gameplay
